/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backend1;

import java.sql.*;

/**
 *
 * @author vali
 */
public class Backend1 {

    String error;
    Connection con;

    public Backend1() {
    }

    public void connect() throws ClassNotFoundException, SQLException, Exception 
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/balaneancristiandb?autoReconnect=true&useSSL=false", "root", "Puihoward_1423"); // nu uitati sa puneti parola corecta de root pe care o aveti setata pe serverul vostru de MySql.
            System.out.println("Successfully connected to the database!");
        }
        catch (ClassNotFoundException cnfe)
        {
            error = "ClassNotFoundException: Nu s-a gasit driverul bazei de date.";
            throw new ClassNotFoundException(error);
        }
        catch (SQLException cnfe)
        {
            error = "SQLException: Nu se poate conecta la baza de date.";
            throw new SQLException(error);
        }
        catch (Exception e)
        {
            error = "Exception: A aparut o exceptie neprevazuta in timp ce se stabilea legatura la baza de date.";
            throw new Exception(error);
        }
    }

    public void connect(String bd) throws ClassNotFoundException, SQLException, Exception 
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + bd, "root", "Puihoward_1423");
        } 
        catch (ClassNotFoundException cnfe)
        {
            error = "ClassNotFoundException: Nu s-a gasit driverul bazei de date.";
            throw new ClassNotFoundException(error);
        }
        catch (SQLException cnfe)
        {
            error = "SQLException: Nu se poate conecta la baza de date.";
            throw new SQLException(error);
        }
        catch (Exception e)
        {
            error = "Exception: A aparut o exceptie neprevazuta in timp ce se stabilea legatura la baza de date.";
            throw new Exception(error);
        }
    }

    public void connect(String bd, String ip) throws ClassNotFoundException, SQLException, Exception
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://" + ip + ":3306/" + bd, "root", "Puihoward_1423");
        }
        catch (ClassNotFoundException cnfe)
        {
            error = "ClassNotFoundException: Nu s-a gasit driverul bazei de date.";
            throw new ClassNotFoundException(error);
        }
        catch (SQLException cnfe)
        {
            error = "SQLException: Nu se poate conecta la baza de date.";
            throw new SQLException(error);
        }
        catch (Exception e)
        {
            error = "Exception: A aparut o exceptie neprevazuta in timp ce se stabilea legatura la baza de date.";
            throw new Exception(error);
        }
    } 

    public void disconnect() throws SQLException {
        try 
        {
            if (con != null)
                con.close();
        } 
        catch (SQLException sqle)
        {
            error = ("SQLException: Nu se poate inchide conexiunea la baza de date.");
            throw new SQLException(error);
        }
    }

    public void adaugaStudent(String nume, String prenume, int varsta, int an) throws SQLException, Exception
    {
        if (con != null)
        {
            try
            {
                // create a prepared SQL statement
                String studentInsertionStatement = "insert into studenti(nume, prenume, varsta, an) values(?,?,?,?)";
                PreparedStatement updateStudents = con.prepareStatement(studentInsertionStatement);
                updateStudents.setString(1, nume);
                updateStudents.setString(2, prenume);
                updateStudents.setInt(3, varsta);
                updateStudents.setInt(4, an);
                updateStudents.executeUpdate();
                System.out.println("Inserted student '" + nume + " " + prenume + "' into the database");
            }
            catch (SQLException sqle)
            {
                error = "ExceptieSQL: Reactualizare nereusita; este posibil sa existe duplicate.";
                throw new SQLException(error);
            }
        } 
        else
        {
            error = "Exceptie: Conexiunea cu baza de date a fost pierduta.";
            throw new Exception(error);
        }
    } // end of adaugaPacient()
    
    public void adaugaDepartament(String numeDepartament, int bugetDepartament) throws SQLException, Exception
    {
        if (con != null) 
        {
            try 
            {
                // create a prepared SQL statement
                String departmentInsertionStatement = "insert into departamente(nume, buget) values(?,?)";
                PreparedStatement updateDepartments = con.prepareStatement(departmentInsertionStatement);
                updateDepartments.setString(1, numeDepartament);
                updateDepartments.setInt(2, bugetDepartament);
                updateDepartments.executeUpdate();
                System.out.println("Inserted department '" + numeDepartament  + "' into the database");
            } 
            catch (SQLException sqle)
            {
                error = "ExceptieSQL: Reactualizare nereusita; este posibil sa existe duplicate.";
                throw new SQLException(error);
            }
        } 
        else 
        {
            error = "Exceptie: Conexiunea cu baza de date a fost pierduta.";
            throw new Exception(error);
        }
    } // end of adaugaMedic()
    
    public void adaugaRelatie(int idStudent, int idDepartament) throws SQLException, Exception
    {
        if (con != null)
        {
            try 
            {
                // create a prepared SQL statement
                String relationInsertionStatement = "insert into studenti_has_departamente(Studenti_idStudent, Departamente_idDepartament) values(?, ?)";
                PreparedStatement updateRelation = con.prepareStatement(relationInsertionStatement);
                updateRelation.setInt(1, idStudent);
                updateRelation.setInt(2, idDepartament);
                updateRelation.executeUpdate();
                // se poate modifica valoarea datei astfel incat sa se ia data curenta a sistemului:
                // stmt.executeUpdate("insert into consultatie(idpacient, idmedic, DataConsultatie, Diagnostic, Medicament) values('" + idpacient + "'  , '" + idmedic + "', CURDATE(), '" + Diagnostic + "', '" + Medicament + "');");        
            }
            catch (SQLException sqle)
            {
                error = "ExceptieSQL: Reactualizare nereusita; este posibil sa existe duplicate.";
                throw new SQLException(error);
            }
        } 
        else 
        {
            error = "Exceptie: Conexiunea cu baza de date a fost pierduta.";
            throw new Exception(error);
        }
    } // end of adaugaConsultatie()

    public ResultSet vedeTabela(String tabel) throws SQLException, Exception
    {
        ResultSet rs = null;
        try
        {
            String queryString = ("select * from `balaneancristiandb`.`" + tabel + "`;");
            Statement stmt = con.createStatement(/*ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
            rs = stmt.executeQuery(queryString);
        } 
        catch (SQLException sqle)
        {
            error = "SQLException: Interogarea nu a fost posibila.";
            throw new SQLException(error);
        }
        catch (Exception e)
        {
            error = "A aparut o exceptie in timp ce se extrageau datele.";
            throw new Exception(error);
        }
        return rs;
    } // vedeTabela()
    
    public ResultSet vedeRelatie() throws SQLException, Exception
    {
        ResultSet rs = null;
        try
        {
            String queryString = ("select a.nume numeStudent, a.prenume prenumeStudent, a.varsta varstaStudent, b.nume numeDepartament, b.buget bugetDepartament, c.Studenti_idStudent idStudentRelatie, c.Departamente_idDepartament idDepartamentRelatie FROM studenti a, departamente b, studenti_has_departamente c WHERE a.idStudent = c.Studenti_idStudent and b.idDepartament = c.Departamente_idDepartament;");
            Statement stmt = con.createStatement(/*ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
            rs = stmt.executeQuery(queryString);
        }
        catch (SQLException sqle)
        {
            error = "SQLException: Interogarea nu a fost posibila.";
            throw new SQLException(error);
        }
        catch (Exception e)
        {
            error = "A aparut o exceptie in timp ce se extrageau datele.";
            throw new Exception(error);
        }
        return rs;
    } // vedeConsultatie()

    public void stergeDateTabela(int[] primaryKeys, String tabela, String dupaID) throws SQLException, Exception 
    {
        if (con != null)
        {
            try 
            {
                // create a prepared SQL statement
                PreparedStatement delete;
                delete = con.prepareStatement("DELETE FROM " + tabela + " WHERE " + dupaID + "=?;");
                for (int i = 0; i < primaryKeys.length; i++)
                {
                    delete.setInt(1, primaryKeys[i]);
                    delete.execute();
                }
            } 
            catch (SQLException sqle)
            {
                error = "ExceptieSQL: Reactualizare nereusita; este posibil sa existe duplicate.";
                throw new SQLException(error);
            } 
            catch (Exception e)
            {
                error = "A aparut o exceptie in timp ce erau sterse inregistrarile.";
                throw new Exception(error);
            }
        } 
        else
        {
            error = "Exceptie: Conexiunea cu baza de date a fost pierduta.";
            throw new Exception(error);
        }
    } // end of stergeDateTabela()

    public void stergeTabela(String tabela) throws SQLException, Exception
    {
        if (con != null)
        {
            try
            {
                // create a prepared SQL statement
                Statement stmt;
                stmt = con.createStatement();
                stmt.executeUpdate("delete from " + tabela + ";");
            } 
            catch (SQLException sqle)
            {
                error = "ExceptieSQL: Stergere nereusita; este posibil sa existe duplicate.";
                throw new SQLException(error);
            }
        } 
        else
        {
            error = "Exceptie: Conexiunea cu baza de date a fost pierduta.";
            throw new Exception(error);
        }
    } // end of stergeTabela()

    public void modificaTabela(String tabela, String IDTabela, int ID, String[] campuri, String[] valori) throws SQLException, Exception
    {
        String update = "update " + tabela + " set ";
        String temp = "";
        if (con != null)
        {
            try
            {
                for (int i = 0; i < campuri.length; i++) 
                {
                    if (i != (campuri.length - 1))
                    {
                        temp = temp + campuri[i] + "='" + valori[i] + "', ";
                    }
                    else
                    {
                        temp = temp + campuri[i] + "='" + valori[i] + "' where " + IDTabela + " = '" + ID + "';";
                    }
                }
                update = update + temp;
                // create a prepared SQL statement
                Statement stmt;
                stmt = con.createStatement();
                stmt.executeUpdate(update);
            } 
            catch (SQLException sqle)
            {
                error = "ExceptieSQL: Reactualizare nereusita; este posibil sa existe duplicate.";
                throw new SQLException(error);
            }
        } 
        else 
        {
            error = "Exceptie: Conexiunea cu baza de date a fost pierduta.";
            throw new Exception(error);
        }
    } // end of modificaTabela()

    public ResultSet intoarceLinie(String tabela, int ID) throws SQLException, Exception
    {
        ResultSet rs = null;
        try
        {
            // Execute query
            String queryString = ("SELECT * FROM " + tabela + " where idpacient=" + ID + ";");
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(queryString); //sql exception
        } 
        catch (SQLException sqle)
        {
            error = "SQLException: Interogarea nu a fost posibila.";
            throw new SQLException(error);
        }
        catch (Exception e)
        {
            error = "A aparut o exceptie in timp ce se extrageau datele.";
            throw new Exception(error);
        }
        return rs;
    } // end of intoarceLinie()

    public ResultSet intoarceLinieDupaId(String tabela, String denumireId, int ID) throws SQLException, Exception 
    {
        ResultSet rs = null;
        try
        {
            // Execute query
            String queryString = ("SELECT * FROM " + tabela + " where " + denumireId + "=" + ID + ";");
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(queryString); //sql exception
        } 
        catch (SQLException sqle)
        {
            error = "SQLException: Interogarea nu a fost posibila.";
            throw new SQLException(error);
        }
        catch (Exception e)
        {
            error = "A aparut o exceptie in timp ce se extrageau datele.";
            throw new Exception(error);
        }
        return rs;
    } // end of intoarceLinieDupaId()
    
    public ResultSet intoarceRelatieId(int ID) throws SQLException, Exception
    {
        ResultSet rs = null;
        try
        {
            // Execute query
            String queryString = ("SELECT a.nume numeStudent, a.prenume prenumeStudent, a.varsta varstaStudent, b.nume numeDepartament, b.buget bugetDepartament,c.Studenti_idStudent idStudentRelatie,c.Departamente_idDepartament idDepartamentRelatie FROM studenti a, departamente b, studenti_has_departamente c WHERE a.idStudent = c.Studenti_idStudent AND b.idDepartament = c.Departamente_idDepartament and idLegatura = '" + ID + "';");
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(queryString); //sql exception
        } 
        catch (SQLException sqle)
        {
            error = "SQLException: Interogarea nu a fost posibila.";
            throw new SQLException(error);
        } 
        catch (Exception e)
        {
            error = "A aparut o exceptie in timp ce se extrageau datele.";
            throw new Exception(error);
        }
        return rs;
    } // end of intoarceLinieDupaId()
}